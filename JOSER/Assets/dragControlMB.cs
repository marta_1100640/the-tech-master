﻿using UnityEngine;
using System.Collections;

public class dragControlMB : MonoBehaviour {
    private bool rodar, deslocar;
    public bool colliderMotherboard;
    public static bool completo=false;
    private Vector3 posRato, pontoEcra;
    Rigidbody rb;

    // Use this for initialization
    void Start () {
        rb = gameObject.GetComponent<Rigidbody>();
	}

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.name == "StormScout2")
        {
            if (gameObject.transform.eulerAngles.x > 270 && gameObject.transform.eulerAngles.x < 300)
            {
                colliderMotherboard = true;
            }
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.gameObject.name == "StormScout2")
        {
            colliderMotherboard = false;
        }
    }

    void OnMouseDrag()
    {
        pontoEcra = new Vector3(Input.mousePosition.x, Input.mousePosition.y, gameObject.transform.localPosition.z);
        rb.useGravity = false;
        rb.freezeRotation = true;
        float distX = pontoEcra.x - posRato.x;
        float distY = pontoEcra.y - posRato.y;
        if (deslocar) rb.MovePosition(new Vector3(gameObject.transform.position.x + (distX * 0.005f), gameObject.transform.position.y + (distY * 0.005f), gameObject.transform.position.z));
        if (rodar)
        {
            gameObject.transform.eulerAngles = new Vector3(gameObject.transform.eulerAngles.x + (distY * 0.5f), gameObject.transform.eulerAngles.y + (distX * -1 * 0.5f), 0);
        }
        if (Input.GetAxis("Mouse ScrollWheel") > 0) rb.MovePosition(new Vector3(gameObject.transform.position.x, gameObject.transform.position.y, gameObject.transform.position.z + 0.1f));
        if (Input.GetAxis("Mouse ScrollWheel") < 0) rb.MovePosition(new Vector3(gameObject.transform.position.x, gameObject.transform.position.y, gameObject.transform.position.z - 0.1f));
    }

    void OnMouseUp()
    {
        if (colliderMotherboard)
        {
            //GameObject.Find("ScoreController").GetComponent<ScoreController>().score++;
            completo = true;
            //Debug.Log("Nível completo.");
            rb.MovePosition(new Vector3(-3.217f, 5.81f, 15.463f));
            gameObject.transform.eulerAngles = new Vector3(-90f, -24.8426f, 0f);
            gameObject.GetComponent<Rigidbody>().useGravity = false;
            gameObject.GetComponent<Rigidbody>().freezeRotation = true;

            //winPanel.SetActive(true);
        }
    }

	// Update is called once per frame
	void Update () {
        if (completo)
        {
            gameObject.GetComponent<Rigidbody>().useGravity = false;
            gameObject.GetComponent<Rigidbody>().freezeRotation = true;
        }
        else
        {
            posRato = new Vector3(Input.mousePosition.x, Input.mousePosition.y, gameObject.transform.localPosition.z);
            if (!Input.GetMouseButton(0) && !gameObject.GetComponent<Rigidbody>().useGravity)
            {
                gameObject.GetComponent<Rigidbody>().useGravity = true;
                gameObject.GetComponent<Rigidbody>().freezeRotation = false;
            }
            if (Input.GetMouseButton(0))
            {
                deslocar = true;
                if (Input.GetKey(KeyCode.E))
                {
                    deslocar = false;
                    rodar = true;
                }
                if (Input.GetKeyUp(KeyCode.E))
                {
                    deslocar = true;
                    rodar = false;
                }
            }
            else
            {
                deslocar = false;
                rodar = false;
            }
        }
	}
}
