﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;

public class ScoreControllerNivel2 : MonoBehaviour {

    public static int score = 0;
    private bool done1 = false, done2 = false;
    public GameObject ScoreTexto;
    public GameObject WinTexto;
    public GameObject WinPainel;
    private Text texto1, texto2;

	// Use this for initialization
	void Start () {
	    
	}
	
	// Update is called once per frame
	void Update () {
	    if(dragControlScrew.parafusosTirados){
            if (!done1)
            {
                done1 = true;
                score += 1;
                texto1 = ScoreTexto.GetComponent<Text>();
                texto1.text = "Score: " + score.ToString() + "/2";
            }
        }

        if (dragControlMB.completo)
        {
            if (!done2)
            {
                done2 = true;
                score += 1;
                texto1.text = "Score: " + score.ToString() + "/2";
            }
        }

        if (done1 && done2)
        {
            texto2 = WinTexto.GetComponent<Text>();
            if (contagemTempo.minutos == 0 && contagemTempo.segundos < 30)
            {
                texto2.text = "Congratulations! You were very fast!";
            }
            else if (contagemTempo.minutos == 0 && contagemTempo.segundos > 30 && contagemTempo.segundos < 59)
            {
                texto2.text = "That was great! You where a bit slow though!";
            }
            else
            {
                texto2.text = "You did it! But you should try a bit faster next time!";
            }

            WinPainel.SetActive(true);
        }
	}
}
