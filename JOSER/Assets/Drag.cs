﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Drag : MonoBehaviour {
    public int Score = 0;

	// Use this for initialization
	void Start () {
        GameObject slot = GameObject.Find("Group 676");
        slot.GetComponent<Renderer>().material.color = new Color(255, 0, 0);
        GameObject slot2 = GameObject.Find("Group 684");
        slot2.GetComponent<Renderer>().material.color = new Color(255, 0, 0);
	}

    void OnMouseDrag()
    {
        Vector3 ponto = new Vector3(Input.mousePosition.x, Input.mousePosition.y, 0);
        Ray raio = Camera.main.ScreenPointToRay(ponto);
        RaycastHit[] hits = Physics.RaycastAll(raio);
        if (hits.Length > 0)
        {
            for (int i = 0; i < hits.Length; i++)
            {
                if (hits[i].collider.gameObject.name == "Asus+Gene+ITX")
                {
                    GameObject slot = GameObject.Find("Group 676");
                    slot.GetComponent<Renderer>().material.color = new Color(0, 255, 0);
                }
            }
        }
    }

    void OnMouseUp()
    {
        Vector3 ponto = new Vector3(Input.mousePosition.x, Input.mousePosition.y, 0);
        Ray raio = Camera.main.ScreenPointToRay(ponto);
        RaycastHit[] hits = Physics.RaycastAll(raio);
        if (hits.Length > 0)
        {
            for (int i = 0; i < hits.Length; i++)
            {
                //Debug.Log(hits[i].collider.gameObject.name);
                GameObject rs1 = GameObject.Find("Asus+Gene+ITX");
                if (hits[i].collider.gameObject == rs1)
                {
                    gameObject.GetComponent<Rigidbody>().useGravity = false;
                    gameObject.GetComponent<BoxCollider>().isTrigger = true;
                    gameObject.GetComponent<Rigidbody>().velocity = Vector3.zero;
                    gameObject.GetComponent<Rigidbody>().freezeRotation = true;

                    Vector3 pos = new Vector3((hits[i].collider.transform.position.x + (float)0.26), hits[i].collider.transform.position.y + (float)0.1, hits[i].collider.transform.position.z + (float)0.05);
                    gameObject.transform.position = pos;
                    //gameObject.transform.position = Vector3.Lerp(transform.position, pos, Time.deltaTime * 2);
                    //gameObject.transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.Euler(0, 90, 0), Time.deltaTime * 2);
                    gameObject.transform.eulerAngles = new Vector3(0, 90, 0);
                    GameObject texto = GameObject.Find("FPSController/FirstPersonCharacter/Canvas/ScoreText");
                    //Debug.Log(texto);
                    Score++;
                    texto.GetComponent<Text>().text = "Score: "+Score;
                }                    
            }
        }

    }
	
	// Update is called once per frame
	void Update () {
            /*TextMesh specs = GameObject.Find("Specs").GetComponent<TextMesh>();
            specs.text = "RAM";*/
	}
}