﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class ScoreController : MonoBehaviour {
    public int score = 0;
    public int currentTime;

	// Use this for initialization
	void Start () {
        
	}
	
	// Update is called once per frame
	void Update () {
        currentTime = (int)Time.time;
        if (SceneManager.GetActiveScene().name == "Nivel1" && score == 1)
        {
            score = 0;
            SceneManager.LoadScene("Nivel2");
        }
        if (SceneManager.GetActiveScene().name == "Nivel2" && score == 1)
        {
            //Debug.Log("Nivel completo!");
        }
	}
}
