﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ScoreControllerN1 : MonoBehaviour
{
    public int currentScore = 0, currentSeconds = 0, currentMinutes = 0;
    public GameObject scorePanel, timePanel, winPanel, winText;

    // Use this for initialization
    void Start()
    {
        
    }

    public void IncreaseScore()
    {
        currentScore++;
        scorePanel.GetComponent<Text>().text = "Score: " + currentScore+"/1";
    }

    // Update is called once per frame
    void Update()
    {
        currentSeconds = (int)Time.time;
        if (currentSeconds == 60)
        {
            currentSeconds = 0;
            currentMinutes++;
        }
        timePanel.GetComponent<Text>().text = currentMinutes + ":" + currentSeconds;
        if (currentScore == 1)
        {
            if (currentMinutes == 0 && currentSeconds <= 30) winText.GetComponent<Text>().text = "Congratulations! You were very fast!";
            if (currentMinutes == 0 && (currentSeconds > 30 && currentSeconds <= 60)) winText.GetComponent<Text>().text = "That was great! You were a bit slow though!";
            if (currentMinutes >= 1) winText.GetComponent<Text>().text = "You did it! You should try a bit faster next time!";
            winPanel.SetActive(true);
            currentScore = 0;
        }
    }
}
