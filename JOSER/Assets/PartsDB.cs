﻿using UnityEngine;
using System.Collections;

public class PartsDB : MonoBehaviour {

	// Use this for initialization
	void Start () {
        
	}
	
    public string[] getSpecs(string name){
        string[] desc = new string[3];
        switch (name)
        {
            case "ram":
                desc[0] = "Kingston RAM";
                desc[1] = "4gb";
                desc[2] = "35";
                break;
            case "ram(Clone)":
                desc[0] = "Kingston RAM";
                desc[1] = "4gb";
                desc[2] = "35";
                break;
            case "Asus+Gene+ITX":
                desc[0] = "Asus Gene";
                desc[1] = "LGA1151 DDR4";
                desc[2] = "200";
                break;
            case "Asus+Gene+ITX(Clone)":
                desc[0] = "Asus Gene";
                desc[1] = "LGA1151 DDR4";
                desc[2] = "200";
                break;
        }
        return desc;
    }

    public int getTotalPrice()
    {
        int total = 0;
        GameObject[] pecas = GameObject.FindGameObjectsWithTag("hardware");
        foreach (GameObject peca in pecas)
        {
            string preco = GameObject.Find("PartsController").GetComponent<PartsDB>().getSpecs(peca.name)[2];
            total += int.Parse(preco);
        }
        return total;
    }

	// Update is called once per frame
	void Update () {
	
	}
}
