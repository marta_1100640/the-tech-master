﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;

public class dragControlScrew : MonoBehaviour {

    private bool rodar, deslocar;
    public GameObject PainelText;
    private TextMesh texto1;
    private Vector3 posRato, pontoEcra;
    Rigidbody rb;
    private int contaParafusos = 0;
    public static bool parafusosTirados=false;

    // Use this for initialization
    void Start()
    {
        rb = gameObject.GetComponent<Rigidbody>();
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.name=="Screw1" || other.gameObject.name == "Screw2" || other.gameObject.name == "Screw3" || other.gameObject.name == "Screw4" || other.gameObject.name == "Screw5" || other.gameObject.name == "Screw6" || other.gameObject.name == "Screw7" || other.gameObject.name == "Screw8")
        {
            GameObject.FindWithTag(other.gameObject.name).SetActive(false);
            contaParafusos += 1;
        }

        if (contaParafusos == 8)
        {
            parafusosTirados = true;
            texto1 = PainelText.GetComponent<TextMesh>();
            texto1.text = "Place the motherboard\ninside the case in a vertical\nposition, facing outwards";
        }
    }

    void OnMouseDrag()
    {
        pontoEcra = new Vector3(Input.mousePosition.x, Input.mousePosition.y, gameObject.transform.localPosition.z);
        rb.useGravity = false;
        rb.freezeRotation = true;
        float distX = pontoEcra.x - posRato.x;
        float distY = pontoEcra.y - posRato.y;
        if (deslocar) rb.MovePosition(new Vector3(gameObject.transform.position.x + (distX * 0.005f), gameObject.transform.position.y + (distY * 0.005f), gameObject.transform.position.z));
        if (rodar)
        {
            gameObject.transform.eulerAngles = new Vector3(gameObject.transform.eulerAngles.x + (distY * 0.5f), gameObject.transform.eulerAngles.y + (distX * -1 * 0.5f), 0);
        }
        if (Input.GetAxis("Mouse ScrollWheel") > 0) rb.MovePosition(new Vector3(gameObject.transform.position.x, gameObject.transform.position.y, gameObject.transform.position.z + 0.1f));
        if (Input.GetAxis("Mouse ScrollWheel") < 0) rb.MovePosition(new Vector3(gameObject.transform.position.x, gameObject.transform.position.y, gameObject.transform.position.z - 0.1f));
    }

    void OnMouseUp()
    {

    }

    // Update is called once per frame
    void Update()
    {
            posRato = new Vector3(Input.mousePosition.x, Input.mousePosition.y, gameObject.transform.localPosition.z);
            if (!Input.GetMouseButton(0) && !gameObject.GetComponent<Rigidbody>().useGravity)
            {
                gameObject.GetComponent<Rigidbody>().useGravity = true;
                gameObject.GetComponent<Rigidbody>().freezeRotation = false;
            }
            if (Input.GetMouseButton(0))
            {
                deslocar = true;
                if (Input.GetKey(KeyCode.E))
                {
                    deslocar = false;
                    rodar = true;
                }
                if (Input.GetKeyUp(KeyCode.E))
                {
                    deslocar = true;
                    rodar = false;
                }
            }
            else
            {
                deslocar = false;
                rodar = false;
            }
    }
}
