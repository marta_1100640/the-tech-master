﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class botaoMenu : MonoBehaviour
{

    [SerializeField]
    private string m_SceneToLoad;      // The name of the scene to load.

    // Use this for initialization
    void Start()
    {

    }

    public void ExitPress()
    {
        Application.Quit();
    }

    public void StartLevel()
    {
        //Application.LoadLevel(1);

        if (m_SceneToLoad == "EXIT")
            Application.Quit();
        else
            SceneManager.LoadScene(m_SceneToLoad, LoadSceneMode.Single);
    }

    // Update is called once per frame
    void Update()
    {

    }
}