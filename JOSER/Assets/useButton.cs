﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class useButton : MonoBehaviour, IPointerDownHandler {

    public GameObject screw;
    public GameObject toolbox;

    public void OnPointerDown(PointerEventData eventData)
    {
        toolbox.SetActive(false);
        screw.SetActive(true);
        MouseLook.pause = false;
        //Debug.Log(eventData.selectedObject.name);
    }

    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
