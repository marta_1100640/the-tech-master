﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;

public class botaofechar : MonoBehaviour, IPointerDownHandler{

    public GameObject painel, painelPreco, textoPreco;
    public GameObject tempo;
    public static bool iniciarTempo = false;
    private int tempoInicial;
    private float tempofinal = 0.0f;
    private Text texto;

    public void OnPointerDown(PointerEventData eventData)
    {
        painel.SetActive(false);
        if (SceneManager.GetActiveScene().name == "Nivel5")
        {
            painelPreco.SetActive(true);
            updatePriceGUI();
        }

        iniciarTempo = true;
        //Debug.Log(eventData.selectedObject.name);
    }

    public void updatePriceGUI()
    {
        textoPreco.GetComponent<Text>().text = "Preço total: " + GameObject.Find("PartsController").GetComponent<PartsDB>().getTotalPrice() + "€";
    }

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        /*if (iniciarTempo)
        {
            tempofinal += Time.deltaTime;
            texto = tempo.GetComponent<Text>();
            texto.text = tempofinal.ToString();
            Debug.Log(tempofinal.ToString());
        }*/
	}
}
