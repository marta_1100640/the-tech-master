﻿using UnityEngine;
using System.Collections;

public class InstanciateTest : MonoBehaviour {
    public GameObject GUIButton;

    void OnMouseDown()
    {
        GameObject ram = GameObject.Find("ram");
        Instantiate(ram, gameObject.transform.position, Quaternion.identity);
        GUIButton.GetComponent<botaofechar>().updatePriceGUI();
    }

	// Use this for initialization
	void Start () {
        
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
