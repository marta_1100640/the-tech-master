﻿using UnityEngine;
using System.Collections;

public class dragControlCase : MonoBehaviour {

    private bool rodar, deslocar;
    public bool colliderScrew;
    private Vector3 posRato, pontoEcra;
    Rigidbody rb;
    private int contaParafusos = 0;
    public static bool parafusosTirados = false;

    // Use this for initialization
    void Start()
    {
        rb = gameObject.GetComponent<Rigidbody>();
    }

    void OnMouseDrag()
    {
        if (dragControlScrew.parafusosTirados)
        {
            pontoEcra = new Vector3(Input.mousePosition.x, Input.mousePosition.y, gameObject.transform.localPosition.z);
            rb.useGravity = false;
            rb.freezeRotation = true;
            rb.constraints = RigidbodyConstraints.None;
            float distX = pontoEcra.x - posRato.x;
            float distY = pontoEcra.y - posRato.y;
            if (deslocar) rb.MovePosition(new Vector3(gameObject.transform.position.x + (distX * 0.005f), gameObject.transform.position.y + (distY * 0.005f), gameObject.transform.position.z));
            if (rodar)
            {
                gameObject.transform.eulerAngles = new Vector3(gameObject.transform.eulerAngles.x + (distY * 0.5f), gameObject.transform.eulerAngles.y + (distX * -1 * 0.5f), 0);
            }
            if (Input.GetAxis("Mouse ScrollWheel") > 0) rb.MovePosition(new Vector3(gameObject.transform.position.x, gameObject.transform.position.y, gameObject.transform.position.z + 0.1f));
            if (Input.GetAxis("Mouse ScrollWheel") < 0) rb.MovePosition(new Vector3(gameObject.transform.position.x, gameObject.transform.position.y, gameObject.transform.position.z - 0.1f));
        }
    }

    void OnMouseUp()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (dragControlScrew.parafusosTirados)
        {
            posRato = new Vector3(Input.mousePosition.x, Input.mousePosition.y, gameObject.transform.localPosition.z);
            if (!Input.GetMouseButton(0) && !gameObject.GetComponent<Rigidbody>().useGravity)
            {
                gameObject.GetComponent<Rigidbody>().useGravity = true;
                gameObject.GetComponent<Rigidbody>().freezeRotation = false;
            }
            if (Input.GetMouseButton(0))
            {
                deslocar = true;
                if (Input.GetKey(KeyCode.E))
                {
                    deslocar = false;
                    rodar = true;
                }
                if (Input.GetKeyUp(KeyCode.E))
                {
                    deslocar = true;
                    rodar = false;
                }
            }
            else
            {
                deslocar = false;
                rodar = false;
            }
        }
    }
}
