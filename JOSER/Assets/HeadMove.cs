﻿using UnityEngine;
using System.Collections;

public class HeadMove : MonoBehaviour {

    public int Boundary = 50; // distance from edge scrolling starts
    //public int speed = 5;
    private int theScreenWidth;
    private int theScreenHeight;
    public GameObject sphere;
    public float speed = 0.5f;
    public Transform target;

    // Use this for initialization
    void Start () {
        theScreenWidth = Screen.width;
        theScreenHeight = Screen.height;
    }
	
	// Update is called once per frame
	void Update () {
        /*if (Input.mousePosition.x > theScreenWidth - Boundary)
        {
            cam.transform.position.x += speed * Time.deltaTime; // move on +X axis
            
        }
        if (Input.mousePosition.x < 0 + Boundary)
        {
            cam.transform.position.x -= speed * Time.deltaTime; // move on -X axis
        }
        if (Input.mousePosition.y > theScreenHeight - Boundary)
        {
            cam.transform.position.z += speed * Time.deltaTime; // move on +Z axis
        }
        if (Input.mousePosition.y < 0 + Boundary)
        {
            cam.transform.position.z -= speed * Time.deltaTime; // move on -Z axis
        }*/

        if (Input.mousePosition.x > theScreenWidth - Boundary)
        {
            transform.LookAt(target);
            transform.RotateAround(target.position, Vector3.up, speed);
        }
        else if(Input.mousePosition.x < 0 + Boundary)
        {
            transform.LookAt(target);
            transform.RotateAround(target.position, Vector3.down, speed);
        }
        else if (Input.mousePosition.y > theScreenHeight - Boundary)
        {
            /*transform.LookAt(target);
            transform.RotateAround(target.position, Vector3.left, speed);*/
            //Camera.main.transform.rotation.;
        }
        else if (Input.mousePosition.y < 0 + Boundary)
        {
            transform.LookAt(target);
            transform.RotateAround(target.position, Vector3.right, speed);
        }
    }

    void OnGUI()
    {
        /*GUI.Box(Rect((Screen.width / 2) - 140, 5, 280, 25), "Mouse Position = " + Input.mousePosition);
        GUI.Box(Rect((Screen.width / 2) - 70, Screen.height - 30, 140, 25), "Mouse X = " + Input.mousePosition.x);
        GUI.Box(Rect(5, (Screen.height / 2) - 12, 140, 25), "Mouse Y = " + Input.mousePosition.y);*/
    }
}
