﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ScoreControllerN3 : MonoBehaviour
{
    public int currentScore = 0, currentSeconds = 0, currentMinutes = 0;
    public GameObject scorePanel, /*timePanel,*/ winPanel, winText;
    public string which = "";

    // Use this for initialization
    void Start()
    {

    }

    public void IncreaseScore()
    {
        currentScore++;
        scorePanel.GetComponent<Text>().text = "Score: " + currentScore + "/2";
    }

    // Update is called once per frame
    void Update()
    {
        //currentSeconds = (int)Time.time;
        //if (currentSeconds == 60)
        //{
        //    currentSeconds = 0;
        //    currentMinutes++;
        //}
        //timePanel.GetComponent<Text>().text = currentMinutes + ":" + currentSeconds;
        if (currentScore == 2)
        {
            if (currentMinutes == 0 && currentSeconds <= 30 && which == "A")
                winText.GetComponent<Text>().text = "Congratulations! You were very fast!\nThat's not the adequate card though...";
            if (currentMinutes == 0 && currentSeconds <= 30 && which == "Ati+HD5870")
                winText.GetComponent<Text>().text = "Congratulations! You were very fast!\nNice choice of card too!";
            if (currentMinutes == 0 && (currentSeconds > 30 && currentSeconds <= 60) && which == "A")
                winText.GetComponent<Text>().text = "That was great! You were a bit slow though!\nThat's not the adequate card...";
            if (currentMinutes == 0 && (currentSeconds > 30 && currentSeconds <= 60) && which == "Ati+HD5870")
                winText.GetComponent<Text>().text = "That was great! You were a bit slow though!\nNice choice of card!";
            if (currentMinutes >= 1 && which == "A")
                winText.GetComponent<Text>().text = "You did it! You should try a bit faster next time!\nThat's not the adequate card though...";
            if (currentMinutes >= 1 && which == "Ati+HD5870")
                winText.GetComponent<Text>().text = "You did it! You should try a bit faster next time!\nNice choice of card!";
            winPanel.SetActive(true);
            currentScore = 0;
        }
    }
}
