﻿using UnityEngine;
using System.Collections;

public class MouseLook : MonoBehaviour {
    public Vector3 posRato;
    private Camera cam;
    private int inc = 2;
    public static bool pause = false;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        if (botaofechar.iniciarTempo)
        {
            posRato = Camera.main.ScreenToViewportPoint(Input.mousePosition);
            cam = Camera.main;
            if (posRato.x <= 0.05 /*&& cam.transform.eulerAngles.y < 30 || cam.transform.eulerAngles.y > 330*/) cam.transform.eulerAngles = new Vector3(
                cam.transform.eulerAngles.x,
                cam.transform.eulerAngles.y - inc,
                cam.transform.eulerAngles.z);
            if (posRato.x >= 0.95 && cam.transform.eulerAngles.y < 70 || cam.transform.eulerAngles.y > 330) cam.transform.eulerAngles = new Vector3(
                cam.transform.eulerAngles.x,
                cam.transform.eulerAngles.y + inc,
                cam.transform.eulerAngles.z);
            if (posRato.y <= 0.1 && cam.transform.eulerAngles.x < 50) cam.transform.eulerAngles = new Vector3(
                cam.transform.eulerAngles.x + inc,
                cam.transform.eulerAngles.y,
                cam.transform.eulerAngles.z);
            if (posRato.y >= 0.9 && cam.transform.eulerAngles.x > 5) cam.transform.eulerAngles = new Vector3(
                cam.transform.eulerAngles.x - inc,
                cam.transform.eulerAngles.y,
                cam.transform.eulerAngles.z);
        }
	}
}
