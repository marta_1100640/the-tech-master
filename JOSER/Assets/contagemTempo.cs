﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;

public class contagemTempo : MonoBehaviour {

    public GameObject tempo;
    private float temp = 0.0f;
    private Text texto;
    public static int minutos = 0, segundos = 0;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        if (botaofechar.iniciarTempo)
        {
            temp += Time.deltaTime;
            segundos = (int) Math.Truncate(temp);
            texto = tempo.GetComponent<Text>();

            if (segundos < 60 && minutos == 0)
            {
                if (segundos < 10)
                {
                    texto.text = "00:0" + segundos.ToString();
                }
                else
                {
                    texto.text = "00:" + segundos.ToString();
                }  
            }
            else if (segundos == 60)
            {
                minutos += 1;
                temp = 0.0f;
                texto.text = minutos.ToString() + ":00";
            }
            else
            {
                if (segundos < 10)
                {
                    texto.text = minutos.ToString() + ":0" + segundos.ToString();
                }
                else
                {
                    texto.text = minutos.ToString() + ":" + segundos.ToString();
                }
            }
            //texto.text = Math.Truncate(tempofinal).ToString();
            //Debug.Log(tempofinal.ToString());
        }
        else
        {

        }
	}
}
