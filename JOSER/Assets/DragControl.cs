﻿using UnityEngine;
using System.Collections;

public class DragControl : MonoBehaviour {
    private bool rodar, deslocar;
    public bool e1, e2;
    public GameObject p1, p2;
    private Vector3 posRato, pontoEcra;
    Rigidbody rb;

	// Use this for initialization
	void Start () {
        rb = gameObject.GetComponent<Rigidbody>();
	}

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.name == "s1")
        {
            e1 = true;
            p1 = other.gameObject.transform.parent.gameObject;
        }
        if (other.gameObject.name == "s2")
        {
            e2 = true;
            p2 = other.gameObject.transform.parent.gameObject;
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.gameObject.name == "s1")
        {
            e1 = false;
            p1 = null;
        }
        if (other.gameObject.name == "s2")
        {
            e2 = false;
            p2 = null;
        }
    }

    void OnMouseDrag ()
    {
        pontoEcra = new Vector3(Input.mousePosition.x, Input.mousePosition.y, gameObject.transform.localPosition.z);
        rb.useGravity = false;
        rb.freezeRotation = true;
        float distX = pontoEcra.x - posRato.x;
        float distY = pontoEcra.y - posRato.y;
        if(deslocar) rb.MovePosition(new Vector3(gameObject.transform.position.x + (distX * 0.005f), gameObject.transform.position.y + (distY * 0.005f), gameObject.transform.position.z));
        if (rodar)
        {
            gameObject.transform.eulerAngles = new Vector3(0, gameObject.transform.eulerAngles.y + (distX * -1 * 0.5f), gameObject.transform.eulerAngles.z + (distY * 0.5f));
        }
        if (Input.GetAxis("Mouse ScrollWheel") > 0) rb.MovePosition(new Vector3(gameObject.transform.position.x, gameObject.transform.position.y, gameObject.transform.position.z + 0.1f));
        if (Input.GetAxis("Mouse ScrollWheel") < 0) rb.MovePosition(new Vector3(gameObject.transform.position.x, gameObject.transform.position.y, gameObject.transform.position.z - 0.1f));
    }

    void OnMouseUp()
    {
        if ((e1 && e2) && (p1 == p2))
        {
            GameObject.Find("ScoreController").GetComponent<ScoreControllerN1>().IncreaseScore();
            rb.useGravity = false;
            rb.freezeRotation = true;
            //rb.MovePosition(p1.GetComponent<BoxCollider>().transform.position);
        }
    }

    void OnMouseOver()
    {
        if (Input.GetMouseButton(1))
        {
            string[] specs = GameObject.Find("PartsController").GetComponent<PartsDB>().getSpecs(gameObject.name);
            Debug.Log(specs[2]);
        }
    }

	// Update is called once per frame
	void Update () {
        posRato = new Vector3(Input.mousePosition.x, Input.mousePosition.y, gameObject.transform.localPosition.z);
        if (!Input.GetMouseButton(0) && !gameObject.GetComponent<Rigidbody>().useGravity)
        {
            gameObject.GetComponent<Rigidbody>().useGravity = true;
            gameObject.GetComponent<Rigidbody>().freezeRotation = false;
        }
        if (Input.GetMouseButton(0))
        {
            deslocar = true;
            if (Input.GetKey(KeyCode.E))
            {
                deslocar = false;
                rodar = true;
            }
            if (Input.GetKeyUp(KeyCode.E))
            {
                deslocar = true;
                rodar = false;
            }
        }
	}
}
